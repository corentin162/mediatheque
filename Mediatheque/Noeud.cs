﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediatheque
{
    class Noeud<T>
    {
        public T Valeur { get; set; }
        public Noeud<T> Suivant { get; set; }
        public Noeud<T> Precedent { get; set; }

        public Noeud(T valeur)
        {
            Valeur = valeur;
        }

    }


    

    
}
