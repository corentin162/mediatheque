﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediatheque
{
    class Liste<T>
    {
        public int Nombre { get; set; }
        private Noeud<T> premier;
        private Noeud<T> dernier;

        public T this[int indice]
        {
            get
            {
                if (indice < 0 || indice >= Nombre)
                {
                    throw new IndexOutOfRangeException();
                }
                Noeud<T> n = NoeudA(indice);
                return n.Valeur;
            }
            set
            {
                if (indice < 0 || indice >= Nombre)
                {
                    throw new IndexOutOfRangeException();
                }
                Noeud<T> n = NoeudA(indice);
                n.Valeur = value;
            }
        }

        public void Ajouter(T valeur)
        {
            // Ajoute un élément à la fin de la liste
            Noeud<T> n = new Noeud<T>(valeur);

            if (Nombre == 0)
            {
                premier = n;
            }
            else
            {
                n.Precedent = dernier;
                dernier.Suivant = n;
            }
            dernier = n;
            Nombre++;
        }

        private Noeud<T> NoeudA(int indice)
        {
            Noeud<T> n = premier;
            for (int i = 0; i < indice; i++)
            {
                n = n.Suivant;
            }
            return n;
        }

        private Noeud<T> NoeudElement(T valeur)
        {
            Noeud<T> n = premier;
            int i;
            for (i = 0; i < Nombre && !n.Valeur.Equals(valeur); i++)
            {
                n = n.Suivant;
            }
            if (i == Nombre)
            {
                n = null;
            }
            return n;
        }

        public void Inserer(int indice, T valeur)
        {
            if (indice < 0 || indice > Nombre)
            {
                throw new IndexOutOfRangeException();
            }

            Noeud<T> n = new Noeud<T>(valeur);

            if (indice == 0)
            {
                if (Nombre != 0)
                {
                    premier.Precedent = n;
                    n.Suivant = premier;
                }
                premier = n;
            }
            if (indice == Nombre)
            {
                if (Nombre != 0)
                {
                    dernier.Suivant = n;
                    n.Precedent = dernier;
                }
                dernier = n;
            }
            if (indice > 0 && indice < Nombre)
            {
                Noeud<T> precedent;
                Noeud<T> suivant;

                suivant = NoeudA(indice);
                precedent = suivant.Precedent;

                n.Precedent = precedent;
                precedent.Suivant = n;

                n.Suivant = suivant;
                suivant.Precedent = n;
            }
            Nombre++;
        }

        public T ElementA(int indice)
        {
            return NoeudA(indice).Valeur;
        }

        public void Supprimer(T value)
        {
            Noeud<T> noeud = NoeudElement(value);
            if (noeud != null)
            {
                SupprimerNoeud(noeud);
            }
        }

        public void SupprimerA(int indice)
        {
            Noeud<T> noeud = NoeudA(indice);
            SupprimerNoeud(noeud);
        }

        private void SupprimerNoeud(Noeud<T> noeud)
        {
            if (noeud == premier)
            {
                if (Nombre != 0)
                {
                    premier = premier.Suivant;
                    premier.Precedent = null;
                    noeud.Suivant = null;
                }
                else
                {
                    premier = null;
                }
            }
            if (noeud == dernier)
            {
                if (Nombre != 0)
                {
                    dernier = dernier.Precedent;
                    dernier.Suivant = null;
                    noeud.Precedent = null;
                }
                else
                {
                    dernier = null;
                }
            }
            if (noeud != premier && noeud != dernier)
            {
                Noeud<T> precedent = noeud.Precedent;
                Noeud<T> suivant = noeud.Suivant;

                precedent.Suivant = suivant;
                suivant.Precedent = precedent;

                noeud.Suivant = null;
                noeud.Precedent = null;
            }
            Nombre--;
        }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();
            Noeud<T> n = premier;
            strb.Append("{");
            for (int i = 0; i < Nombre; i++)
            {
                T valeur = n.Valeur;
                strb.Append(valeur.ToString());
                strb.Append(", ");
                n = n.Suivant;
            }
            if (Nombre > 0)
            {
                strb.Remove(strb.Length - 2, 2);
            }
            strb.Append("}");
            return strb.ToString();
        }
    }
}

}
