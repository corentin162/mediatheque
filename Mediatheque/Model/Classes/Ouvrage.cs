﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediatheque.Model.Classes
{
    class Ouvrage
    {
        public String Isbn { get; set; }
        public String Titre { get; set; }
        public String Description { get; set; }

    }
}
